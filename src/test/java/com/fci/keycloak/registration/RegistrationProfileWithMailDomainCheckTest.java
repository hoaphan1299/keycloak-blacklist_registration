package com.fci.keycloak.registration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RegistrationProfileWithMailDomainCheckTest {

    private final static String[] validEmails = {"user1@domain.com", "user2@domain3.com", "user1@domain3.net", "user2@domain.net"};
    private final static String[] invalidEmails = {"user1@domain1.com", "user2@domain1.net", "user2@domain2.com", "user1@domain2.net"};
    private Map<String, String> configs = new HashMap<>();

    public RegistrationProfileWithMailDomainCheckTest() {
        configs.put("renewDomains","false");
    }

    RegistrationProfileWithMailDomainCheck defaultRP = new RegistrationProfileWithMailDomainCheck(new ArrayList<>());

    RegistrationProfileWithMailDomainCheck createTestRP(){

        ArrayList<String> domains = new ArrayList<>();
        domains.add("domain1.com");
        domains.add("domain1.net");
        domains.add("domain2.com");
        domains.add("domain2.net");

        return new RegistrationProfileWithMailDomainCheck(domains);
    }

    @Test
    void shouldThrowsDatabaseConnectionError() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            defaultRP.getConnection();
        });
        Assertions.assertThrows(RuntimeException.class, () -> {
            Object rp = new RegistrationProfileWithMailDomainCheck();
        });
    }

    @Test
    void checkGet() {
        Assertions.assertEquals("Profile Validation with Blacklist domain check",defaultRP.getDisplayType());
        Assertions.assertEquals("registration-blacklist-domain",defaultRP.getId());
        Assertions.assertTrue(defaultRP.isConfigurable());
    }

    @Test
    void isValidMailDomain() {
        RegistrationProfileWithMailDomainCheck rp = createTestRP();
        for (String email: validEmails) {
            Assertions.assertTrue(rp.isValidEmail(email, rp.getDomains()));
        }
    }

    @Test
    void isInvalidMailDomain() {
        RegistrationProfileWithMailDomainCheck rp = createTestRP();
        for (String email: invalidEmails) {
            Assertions.assertFalse(rp.isValidEmail(email, rp.getDomains()));
        }
    }

    @Test
    void checkConfig(){
        ProviderConfigProperty config = defaultRP.getConfigProperties().get(0);
        Assertions.assertTrue(config.getType().equalsIgnoreCase("boolean"));
        Assertions.assertTrue(config.getName().equalsIgnoreCase("renewDomains"));
        Assertions.assertTrue(config.getLabel().equalsIgnoreCase("Renew connection"));
        Assertions.assertTrue(config.getHelpText().equalsIgnoreCase("Set to TRUE when add some new domains to the list"));
        Assertions.assertTrue(config.getDefaultValue().toString().equalsIgnoreCase("false"));
    }

    @Test
    void shouldReturnEmptyList() {
        Assertions.assertTrue(defaultRP.getAllDomainMail(null).isEmpty());
    }

    @Test
    void testValidateSuccess(){
        configs.replace("renewDomains","false");
        List<DefaultValidationContext> contexts = new ArrayList<>();
        for (String email: validEmails) {
            contexts.add(new DefaultValidationContext(email,configs));
        }
        RegistrationProfileWithMailDomainCheck rp = createTestRP();
        for (DefaultValidationContext context: contexts) {
            rp.validate(context);
            Assertions.assertTrue(context.getError()==null);
        }
    }

    @Test
    void testValidateError(){
        configs.replace("renewDomains","false");
        List<DefaultValidationContext> contexts = new ArrayList<>();
        for (String email: invalidEmails) {
            contexts.add(new DefaultValidationContext(email,this.configs));
        }
        RegistrationProfileWithMailDomainCheck rp = createTestRP();
        for (DefaultValidationContext context: contexts) {
            rp.validate(context);
            Assertions.assertTrue(context.getError()!=null);
        }
    }

    @Test
    void testTrueConfig(){
        configs.replace("renewDomains","true");
        DefaultValidationContext context = new DefaultValidationContext("test",configs);
        defaultRP.validate(context);
        Assertions.assertTrue(
                context.getAuthenticatorConfig().getConfig().get("renewDomains").equalsIgnoreCase("false")
        );
    }
}