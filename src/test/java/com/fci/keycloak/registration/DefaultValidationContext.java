package com.fci.keycloak.registration;

import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.ResteasyAsynchronousContext;
import org.jboss.resteasy.spi.ResteasyUriInfo;
import org.keycloak.authentication.ValidationContext;
import org.keycloak.common.ClientConnection;
import org.keycloak.events.Event;
import org.keycloak.events.EventBuilder;
import org.keycloak.models.*;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.sessions.AuthenticationSessionModel;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.net.URI;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

public class DefaultValidationContext implements ValidationContext {

    public String email;
    public Map<String, String> configs;
    String error;

    public DefaultValidationContext(String email, Map<String, String> configs) {
        this.email = email;
        this.configs = configs;
    }

    @Override
    public void validationError(MultivaluedMap<String, String> multivaluedMap, List<FormMessage> list) {

    }

    @Override
    public void error(String s) {
        this.error = s;
    }

    @Override
    public void success() {

    }

    @Override
    public void excludeOtherErrors() {

    }

    @Override
    public EventBuilder getEvent() {
        return null;
    }

    @Override
    public EventBuilder newEvent() {
        return null;
    }

    @Override
    public AuthenticationExecutionModel getExecution() {
        return null;
    }

    @Override
    public UserModel getUser() {
        return null;
    }

    @Override
    public void setUser(UserModel userModel) {

    }

    @Override
    public RealmModel getRealm() {
        return null;
    }

    @Override
    public AuthenticationSessionModel getAuthenticationSession() {
        return null;
    }

    @Override
    public ClientConnection getConnection() {
        return null;
    }

    @Override
    public UriInfo getUriInfo() {
        return null;
    }

    @Override
    public KeycloakSession getSession() {
        return null;
    }

    @Override
    public HttpRequest getHttpRequest() {
        return new HttpRequest() {
            @Override
            public HttpHeaders getHttpHeaders() {
                return null;
            }

            @Override
            public MultivaluedMap<String, String> getMutableHeaders() {
                return null;
            }

            @Override
            public InputStream getInputStream() {
                return null;
            }

            @Override
            public void setInputStream(InputStream inputStream) {

            }

            @Override
            public ResteasyUriInfo getUri() {
                return null;
            }

            @Override
            public String getHttpMethod() {
                return null;
            }

            @Override
            public void setHttpMethod(String s) {

            }

            @Override
            public void setRequestUri(URI uri) throws IllegalStateException {

            }

            @Override
            public void setRequestUri(URI uri, URI uri1) throws IllegalStateException {

            }

            @Override
            public MultivaluedMap<String, String> getFormParameters() {
                return null;
            }

            @Override
            public MultivaluedMap<String, String> getDecodedFormParameters() {
                MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
                params.add("email",email);
                return params;
            }

            @Override
            public Object getAttribute(String s) {
                return null;
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public ResteasyAsynchronousContext getAsyncContext() {
                return null;
            }

            @Override
            public boolean isInitial() {
                return false;
            }

            @Override
            public void forward(String s) {

            }

            @Override
            public boolean wasForwarded() {
                return false;
            }
        };
    }

    @Override
    public AuthenticatorConfigModel getAuthenticatorConfig() {
        AuthenticatorConfigModel model = new AuthenticatorConfigModel();
        model.setConfig(configs);
        return model;
    }

    public String getError(){
        return this.error;
    }
}
