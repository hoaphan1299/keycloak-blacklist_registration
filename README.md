# Keycloak - Blocking all of the emails in the blacklist domains from registering new accounts on FPT ID.

## Requirements

- Maven 3.6.3 or above
- JDK 8 or above

## How to compile

```console
$ mvn compile
$ mvn package 
```

## How to run test
```console
$ mvn test
```

## How to install

Create a Dockerfile

```
FROM jboss/keycloak:12.0.4
ADD keycloak-mail-blacklisting-1.0-SNAPSHOT.jar opt/jboss/keycloak/standalone/deployments/
```

Build docker image

```
docker build -t newkeycloak .
```


## Import blacklist domains to DB

Create a new table in MySQL database to store the blacklist domains

```
CREATE TABLE FCI_BLACKLIST( ID int NOT NULL AUTO_INCREMENT, MAIL_DOMAIN varchar(255) NOT NULL, PRIMARY KEY (ID) )
``` 

Import blacklist domains by running the following command:

```
mysql -u keycloak-user -p keycloak < blacklist-emails.sql
```

## How to use

```
- Go to the admin console, in authentication menu. 
- Copy the registration flow
- add a new execution below "Profile Validation" and choose "Profile Validation With Email Domain Check"
- Configure this new execution (otherwise, keycloak will throw error when user register)
- Change the registration binding to this new flow
- Configure the realm to accept registration and verify email (this is important!)
```
